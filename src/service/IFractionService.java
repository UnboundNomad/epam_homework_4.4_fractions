package service;

import model.Fraction;

public interface IFractionService {

    Fraction simplification(Fraction f);

    Fraction addition(Fraction f1, Fraction f2);

    Fraction subtraction(Fraction f1, Fraction f2);

    Fraction multiplication(Fraction f1, Fraction f2);

    Fraction division(Fraction f1, Fraction f2);

}
