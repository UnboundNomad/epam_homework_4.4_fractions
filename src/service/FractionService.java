package service;

import model.Fraction;

public class FractionService implements IFractionService {

    @Override
    public Fraction simplification(Fraction f) {

        int num = f.getNumerator();
        int dem = f.getDenominator();
        int gcd = gcd(num, dem);

        Fraction result = new Fraction(f);

        if (gcd > 1) {
            result.setNumerator(num / gcd);
            result.setDenominator(dem / gcd);
        }

        return result;

    }

    @Override
    public Fraction addition(Fraction f1, Fraction f2) {

        int lcm = lcm(f1.getDenominator(), f2.getDenominator());
        int num = (lcm / f1.getDenominator()) * f1.getNumerator() + (lcm / f2.getDenominator()) * f2.getNumerator();

        Fraction result = new Fraction(num, lcm);

        return simplification(result);
    }

    @Override
    public Fraction subtraction(Fraction f1, Fraction f2) {

        int lcm = lcm(f1.getDenominator(), f2.getDenominator());
        int num = (lcm / f1.getDenominator()) * f1.getNumerator() - (lcm / f2.getDenominator()) * f2.getNumerator();

        Fraction result = new Fraction(num, lcm);

        return simplification(result);
    }

    @Override
    public Fraction multiplication(Fraction f1, Fraction f2) {

        Fraction result = new Fraction(f1.getNumerator() * f2.getNumerator(), f1.getDenominator() * f2.getDenominator());

        return simplification(result);
    }

    @Override
    public Fraction division(Fraction f1, Fraction f2) {

        Fraction result = new Fraction(f1.getNumerator() * f2.getDenominator(), f1.getDenominator() * f2.getNumerator());

        return simplification(result);
    }

    private int gcd(int a, int b) {

        a = Math.abs(a);
        b = Math.abs(b);

        while (a != 0 && b != 0) {
            if (a > b) {
                a %= b;
            } else {
                b %= a;
            }
        }

        return Math.abs(a + b);
    }

    private int lcm(int a, int b) {

        return (a * b) / gcd(a, b);

    }

}
