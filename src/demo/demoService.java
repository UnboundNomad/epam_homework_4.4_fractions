package demo;

import model.Fraction;
import service.IFractionService;

import java.util.concurrent.ThreadLocalRandom;

public class demoService {

    private IFractionService service;

    public demoService(IFractionService service) {
        this.service = service;
    }

    public void execute() {

//        Fraction f1 = new Fraction(11, 19);
//        Fraction f2 = new Fraction(8, 7);

        Fraction f1 = generateFraction(5, 25);
        Fraction f2 = generateFraction(6, 20);

        System.out.printf("Дроби: %s и %s\n", f1, f2);
        System.out.printf("Упрощение: %s и %s\n", service.simplification(f1), service.simplification(f2));
        System.out.printf("%s + %s = %s\n", f1, f2, service.addition(f1, f2));
        System.out.printf("%s - %s = %s\n", f1, f2, service.subtraction(f1, f2));
        System.out.printf("%s * %s = %s\n", f1, f2, service.multiplication(f1, f2));
        System.out.printf("%s / %s = %s\n", f1, f2, service.division(f1, f2));

    }

    private Fraction generateFraction(int min, int max) {

        return new Fraction(ThreadLocalRandom.current().nextInt(min, max / 2), ThreadLocalRandom.current().nextInt(min, max));

    }

}
